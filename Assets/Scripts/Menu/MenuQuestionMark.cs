﻿using UnityEngine;
using System.Collections;

public class MenuQuestionMark : MonoBehaviour {
    public void DeactivateButton() {
        GlobalVar.Instance().enemySpawner.SetActive(true);
        GlobalVar.Instance().highscore.SetActive(false);
        this.gameObject.SetActive(false);
    }
}
