﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {
    public float spawnDistance;
    public float minSpawnDist;
    public float timeToSpawnSmall;
    public float timeToSpawnMed;
    public float timeToSpawnBig;
    float smallSpawnTimer;
    float medSpawnTimer;
    float bigSpawnTimer;
    public GameObject bigMeteorPrefab;
    public GameObject mediumMeteorPrefab;
    public GameObject smallMeteorPrefab;
    void Start () {
        smallSpawnTimer = timeToSpawnSmall;
        medSpawnTimer = timeToSpawnMed;
        bigSpawnTimer = timeToSpawnBig;
	}
	void Update () {
        if(smallSpawnTimer < 0) {
            //spawn
            Vector2 spawnPos = new Vector2(Random.insideUnitCircle.x, Random.insideUnitCircle.y);
            spawnPos *= spawnDistance;
            if(Mathf.Abs(spawnPos.x) < minSpawnDist) {
                if(spawnPos.x > 0) {
                    spawnPos.x = minSpawnDist;
                }
                else {
                    spawnPos.x = -minSpawnDist;
                }
            }
            if(Mathf.Abs(spawnPos.y) < minSpawnDist) {
                if(spawnPos.y > 0) {
                    spawnPos.y = minSpawnDist;
                }
                else {
                    spawnPos.y = -minSpawnDist;
                }
            }
            GameObject.Instantiate(smallMeteorPrefab, spawnPos, transform.rotation);
            smallSpawnTimer = timeToSpawnSmall;
        }
        else {
            smallSpawnTimer -= Time.deltaTime;
        }

        if(medSpawnTimer < 0) {
            //spawn
            Vector2 spawnPos = new Vector2(Random.insideUnitCircle.x, Random.insideUnitCircle.y);
            spawnPos *= spawnDistance;
            if(Mathf.Abs(spawnPos.x)< minSpawnDist) {
                if(spawnPos.x > 0) {
                    spawnPos.x = minSpawnDist;
                }
                else {
                    spawnPos.x = -minSpawnDist;
                }                
            }
            if(Mathf.Abs(spawnPos.y) < minSpawnDist) {
                if(spawnPos.y > 0) {
                    spawnPos.y = minSpawnDist;
                }
                else {
                    spawnPos.y = -minSpawnDist;
                }                
            }
            GameObject.Instantiate(mediumMeteorPrefab, spawnPos, transform.rotation);
            medSpawnTimer = timeToSpawnMed;
        }
        else {
            medSpawnTimer -= Time.deltaTime;
        }

        if(bigSpawnTimer < 0) {
            //spawn
            Vector2 spawnPos = new Vector2(Random.insideUnitCircle.x, Random.insideUnitCircle.y);
            spawnPos *= spawnDistance;
            if(Mathf.Abs(spawnPos.x) < minSpawnDist) {
                if(spawnPos.x > 0) {
                    spawnPos.x = minSpawnDist;
                }
                else {
                    spawnPos.x = -minSpawnDist;
                }
            }
            if(Mathf.Abs(spawnPos.y) < minSpawnDist) {
                if(spawnPos.y > 0) {
                    spawnPos.y = minSpawnDist;
                }
                else {
                    spawnPos.y = -minSpawnDist;
                }
            }
            GameObject.Instantiate(bigMeteorPrefab, spawnPos, transform.rotation);
            bigSpawnTimer = timeToSpawnBig;
        }
        else {
            bigSpawnTimer -= Time.deltaTime;
        }
    }
}
