﻿using UnityEngine;
using System.Collections;

public class Meteor : MonoBehaviour {
    public float speed;
    public GameObject Dip;
    GameObject target;
    public int hitPoints;
    public int damage;
    bool canReach=true;
    public float vegetalTime;
    float vegetalTimer;
    Rigidbody2D myRigid;
    public float height;
	void Start () {
        myRigid = GetComponent<Rigidbody2D>();
        target = GlobalVar.Instance().targetWorld;
    }
    // Update is called once per frame
    void Update() {
        if(!GlobalVar.Instance().enemySpawner.activeSelf) {
            Destroy(this.gameObject);
        }
        if(canReach) {
            transform.position = Vector2.MoveTowards(transform.position, target.transform.position, speed * Time.deltaTime);
        }
        else {
            if(vegetalTimer < 0) {
                vegetalTimer = vegetalTime;
                myRigid.velocity = Vector2.zero;
                canReach = true;
            }
            else {
                vegetalTimer -= Time.deltaTime;
            }
        }  
	}
    public void TakeDamage(int damage) {
        hitPoints -= damage;
        if(hitPoints <= 0) {
            hitPoints = 0;            
            GameObject.Instantiate(Dip, transform.position, transform.rotation);
            Destroy(this.gameObject);
        }
    }
    void OnCollisionEnter2D(Collision2D col) {
        if(col.transform.tag == "warudo") {
            col.transform.GetComponent<RotateWorld>().TakeDamage(damage);
            col.transform.GetComponent<RotateWorld>().PlayMeteorSound();
            Destroy(this.gameObject);
        }
        if(col.transform.tag == "cannon") {
            target.GetComponent<RotateWorld>().TakeDamage(Mathf.RoundToInt(damage/2.0f));
            TakeDamage(3);
        }
    }
    public void Repel(float speed) {
        canReach = false;
        Vector2 dir = -(target.transform.position- transform.position);
        GetComponent<Rigidbody2D>().AddForce(dir *speed* height);
    }
}
