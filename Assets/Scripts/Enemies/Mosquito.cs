﻿using UnityEngine;
using System.Collections;

public class Mosquito : MonoBehaviour {
    public float speed;
    public GameObject target;
    public int hitPoints;
    void Start() {

    }
    // Update is called once per frame
    void Update() {
        transform.position = Vector2.MoveTowards(transform.position, target.transform.position, speed * Time.deltaTime);
    }
    public void TakeDamage(int damage) {
        hitPoints -= damage;
        if(hitPoints <= 0) {
            hitPoints = 0;
            Destroy(this.gameObject);
        }
    }
}
