﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class ScoreUpdater : MonoBehaviour {
    public float updateTime;
    Text scoreText;
    int score;
    float timer;
    public AudioSource source;
    public AudioClip sauceSFX;

	void Start () {
        scoreText = GetComponent<Text>();
        timer = updateTime;
	}
	void Update () {
        if(timer < 0) {
            if(score!= GlobalVar.Instance().score) {
                //animation?
                score = GlobalVar.Instance().score;
                scoreText.text = "" + score;
                source.PlayOneShot(sauceSFX);
            }
            timer = updateTime;
        }
        else {
            timer -= Time.deltaTime;
        }
	}
}
