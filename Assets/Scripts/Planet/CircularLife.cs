﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Advertisements;

public class CircularLife : MonoBehaviour {
    [Header("health variables")]
    public float health;
    public float maxHealth;
    [Header ("life scaling variables")]
    public float maxScale;
    public float minScale;
    [Header("regeneration parameters")]
    public float regenTime;
    float regenerationTimer;
    public float regenAmount;
    public GameObject menu;
	void Start () {
        regenerationTimer = regenTime;
	}
	void Update () {
        if(regenerationTimer < 0) {
            if(health+regenAmount<= maxHealth) {
                health += regenAmount;
                regenerationTimer = regenTime;
                updateVisualStatus();
            }            
        }
        else {
            regenerationTimer -= Time.deltaTime;
        }
	}
    public void TakeDamage(int damage) {
        health -= damage;
        if(health <= 0) {
            health = maxHealth;
            menu.SetActive(true);
            ShowAd();
            GlobalVar.Instance().highscore.SetActive(true);
            if(GlobalVar.Instance().lastHighscore< GlobalVar.Instance().score) {
                GlobalVar.Instance().lastHighscore = GlobalVar.Instance().score;
                GlobalVar.Instance().highscoreText.GetComponent<Text>().text = ": " + GlobalVar.Instance().score;                
            }
            GlobalVar.Instance().score *= 0;
            GlobalVar.Instance().enemySpawner.SetActive(false);
        }
        updateVisualStatus();
    }
    void updateVisualStatus() {
        float updatedScale = (health / maxHealth)*maxScale;
        if(updatedScale<= minScale) {
            transform.localScale = Vector3.one * minScale;
        }
        else {
            transform.localScale = Vector3.one * updatedScale;
        }        
    }
    //
    public void ShowAd()
    {
        if (Advertisement.IsReady())
        {
            Advertisement.Show();
        }
    }

}
