﻿using UnityEngine;
using System.Collections;

public class FaceAnimController : MonoBehaviour {
    Animator anim;
    [Header("Time to change animation")]
    public float timerFrom;
    public float timerTo;
    float changeAnimTimer;
    bool started;
    bool finished;
	void Start () {
        anim = GetComponent<Animator>();
        changeAnimTimer = Random.Range(timerFrom, timerTo);
	}
	void Update () {
        if(GlobalVar.Instance().enemySpawner.activeSelf && !started) {
            started = true;
            finished = false;
        }
        if(!GlobalVar.Instance().enemySpawner.activeSelf && !finished) {
            finished = true;
            started = false;
        }
        if(started) {
            anim.SetTrigger("start");
        }
        if(finished) {
            anim.SetTrigger("reset");
        }
        if(changeAnimTimer < 0) {
            changeAnimTimer = Random.Range(timerFrom, timerTo);
            int animRandom = Random.Range(1, 3);
            anim.SetTrigger("" + animRandom);
        }
        else {
            changeAnimTimer -= Time.deltaTime;
        }
	}
}
