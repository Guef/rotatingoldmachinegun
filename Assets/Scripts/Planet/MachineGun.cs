﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class MachineGun : MonoBehaviour {
    public GameObject spawnPos;
    public GameObject straightBullet;
    public ScreenShake shakeObj;
    public float timeBetweenBullets;
    float bulletTimer=0;
    public RotateWorld rotateScript;
    public float shakeX;
    public float shakeY;
    public float shakeTime;
    public float iterations;
    AudioSource source;
    public AudioClip shootSFX;
    public AudioClip repelSFX;
    float startingPitch;
    public GameObject mozzleFlash;
    bool justShooted;
    public float timeToFlash;
    float flashTimer;
	void Start () {
        source = GetComponent<AudioSource>();
        startingPitch = source.pitch;
        flashTimer = timeToFlash;
    }	
	void Update () {
        if(Input.GetMouseButton(0)) {
            if(bulletTimer < 0) {
                mozzleFlash.SetActive(true);
                justShooted = true;
                shakeObj.SavePrevCamPos();
                shakeObj.Shake(shakeX, shakeY, iterations, shakeTime);
                source.pitch = startingPitch+Random.Range(-0.2f, 0.2f);
                source.PlayOneShot(shootSFX);
                Instantiate(straightBullet, spawnPos.transform.position, spawnPos.transform.rotation);
                bulletTimer = timeBetweenBullets;
            }
            else {                
                bulletTimer -= Time.deltaTime;
            }                        
        }
        if(justShooted) {
            if(flashTimer < 0) {
                mozzleFlash.SetActive(false);
                flashTimer = timeToFlash;
                justShooted = false;
            }
            else {
                flashTimer -= Time.deltaTime;
            }
        }
    }
    void OnCollisionEnter2D(Collision2D col) {
        if(col.transform.tag == "meteor") {
            col.transform.GetComponent<Meteor>().Repel(rotateScript.speed);
            source.PlayOneShot(repelSFX);
        }
        if(col.transform.tag == "dip") {
            col.transform.GetComponent<Diping>().Repel(rotateScript.speed);
        }
    }
}
