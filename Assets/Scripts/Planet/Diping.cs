﻿using UnityEngine;
using System.Collections;

public class Diping : MonoBehaviour {
    GameObject target;
    public float speed;
    bool canReach = true;
    public float vegetalTime;
    float vegetalTimer;
    void Start() {
        target = GlobalVar.Instance().targetWorld;
    }
    void Update() {
        if(!GlobalVar.Instance().enemySpawner.activeSelf) {
            Destroy(this.gameObject);
        }
        if(canReach) {
            transform.position = Vector2.MoveTowards(transform.position, target.transform.position, speed * Time.deltaTime);
        }
        else {
            if(vegetalTimer< 0) {
                vegetalTimer = vegetalTime;
                canReach = true;
            }
            else {
                vegetalTimer -= Time.deltaTime;
            }
        } 
    }
    void OnCollisionEnter2D(Collision2D col) {
        if(col.transform.tag == "warudo") {
            GlobalVar.Instance().score++;
            Destroy(this.gameObject);
        }
    }
    public void Repel(float speed) {
        canReach = false;
        Vector2 dir = -(target.transform.position - transform.position);
        GetComponent<Rigidbody2D>().AddForce(dir * speed);
    }
}
