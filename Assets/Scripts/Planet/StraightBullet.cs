﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class StraightBullet : MonoBehaviour {
    public float speed;
    public float lifeTime;
    public int damage;
    public List<Sprite> bulletFrames;
    SpriteRenderer spriteRender;
    void Awake() {
        spriteRender = GetComponent<SpriteRenderer>();
        spriteRender.sprite = bulletFrames[0];
    }
    void Start() {        
        Destroy(this.gameObject, lifeTime);        
    }
	void Update () {
        transform.localPosition += transform.up*speed;
	}
    void OnCollisionEnter2D(Collision2D col) {
        if(col.transform.tag == "meteor") {
            col.transform.GetComponent<Meteor>().TakeDamage(damage);
            spriteRender.sprite = bulletFrames[1];
            Destroy(this.gameObject);
        }
        if(col.transform.tag == "mosquito") {
            col.transform.GetComponent<Mosquito>().TakeDamage(damage);
        }
    }
}
