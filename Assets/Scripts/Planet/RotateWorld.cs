﻿using UnityEngine;
using System.Collections;

public class RotateWorld : MonoBehaviour {
    //TOUCH STUFF----------------------------------
    float startPos;
    public float minSwipeDistance = 10;
    public float medSwipeDistance = 20;
    public float maxSwipeDistance = 30;
    //Movement-------------------------------------
    [Header("Rotation Speed")]
    public float speed;
    public float macroSpeed;
    public float minSpeed;
    public float medSpeed;
    public float maxSpeed;
    float savedMousePosX = -66666666;
    float savedMousePosY = -66666666;
    //velocidad variada
    //momentum-------------------------------------
    [Header("Momentum variables")]
    public float momentumSpeed1;
    public float momentumSpeed2;
    public float momentumSpeed3;
    public float momentumSpeed4;
    public float momentumSpeed;
    float momentumAcel;
    bool rightMomentum;
    float currentMomentumTimer;
    public float momentumTimer;
    //---------------------------------------------
    //hp?------------------------------------------
    [Header("unrelated variables")]
    public CircularLife lifeManager;
    public AudioSource source;
    public AudioClip meteorSFX;
    //---------------------------------------------
    void Start() {
        source = GetComponent<AudioSource>();
    }
    void Update () {
        if(Input.GetMouseButtonDown(0)) {//starts pressing
            startPos = Input.mousePosition.x;
        }
        
        if(Input.GetMouseButton(0)) {//holds
            float delta = (Input.mousePosition.x - savedMousePosX)+(Input.mousePosition.y - savedMousePosY);
            //
            if(Mathf.Abs(delta) > minSwipeDistance) {
                speed = minSpeed;
                momentumAcel = momentumSpeed2;
            }
            else {
                speed = macroSpeed;
                momentumAcel = momentumSpeed1;
            }
            if(Mathf.Abs(delta) > medSwipeDistance) {
                speed = medSpeed;
                momentumAcel = momentumSpeed3;
            }
             if(Mathf.Abs(delta) > maxSwipeDistance) {
                speed = maxSpeed;
                momentumAcel = momentumSpeed4;
            }

            //
            if(savedMousePosX!= Input.mousePosition.x || savedMousePosY != Input.mousePosition.y) {
                if(delta < 0) {//left swipe
                    transform.Rotate(Vector3.forward * speed);
                    currentMomentumTimer = 0;//ignores momentum
                    rightMomentum = false;
                    savedMousePosX = Input.mousePosition.x;
                    savedMousePosY = Input.mousePosition.y;
                }
                if(delta > 0) {//right swipe
                    transform.Rotate(Vector3.forward * -1 * speed);
                    currentMomentumTimer = 0;
                    rightMomentum = true;
                    savedMousePosX = Input.mousePosition.x;
                    savedMousePosY = Input.mousePosition.y;
                }
            }
            else {
                speed = 0;
            }            
        }
        //Momentum
        if(Input.GetMouseButtonUp(0)) {//Released
            currentMomentumTimer = momentumTimer;//starts momentum
            //momentumAcel = momentumSpeed*speed;
        }
        if(currentMomentumTimer > 0) {//if swipe ended
            currentMomentumTimer -= Time.deltaTime;
            if(momentumAcel > 0) {
                momentumAcel -= Time.deltaTime;
            }
            if(momentumAcel < 0) {
                momentumAcel = 0;//truncating to 0 to avoid reversing momentum
            }
            if(rightMomentum) {                
                transform.Rotate(Vector3.forward * -momentumAcel);
            }
            else {
                transform.Rotate(Vector3.forward * momentumAcel);
            }
        }
	}
    public void TakeDamage(int damage) {
        lifeManager.TakeDamage(damage);
    }
    public void PlayMeteorSound() {
        source.PlayOneShot(meteorSFX);
    }
}
