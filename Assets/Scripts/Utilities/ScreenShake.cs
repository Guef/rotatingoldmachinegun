﻿using UnityEngine;
using System.Collections;

public class ScreenShake : MonoBehaviour {
    //script from -http://newbquest.com/2014/06/the-art-of-screenshake-with-unity-2d-script/
    Vector3 originalCameraPosition;
    float shakeXAmt = 0;
    float shakeYAmt = 0;
    public Camera mainCamera;
    void Awake() {
        originalCameraPosition = transform.position;
    }
    public void Shake(float xAmount, float yAmount, float repeatSeconds, float stopSeconds) {
        shakeXAmt = xAmount;
        shakeYAmt = yAmount;
        InvokeRepeating("CameraShake", 0, repeatSeconds);
        Invoke("StopShaking", stopSeconds);
    }
    void CameraShake() {
        if(shakeXAmt > 0) {
            float quakeYAmt = Random.value * shakeYAmt * 2 - shakeYAmt;
            Vector3 pp = mainCamera.transform.position;
            pp.y += quakeYAmt;
            float quakeXAmt = Random.value * shakeYAmt * 2 - shakeYAmt;
            pp.x += quakeXAmt;
            mainCamera.transform.position = pp;
        }
    }
    public void SavePrevCamPos() {
        originalCameraPosition = transform.position;
    }
    void StopShaking() {
        CancelInvoke("CameraShake");
        mainCamera.transform.position = originalCameraPosition;
    }
}