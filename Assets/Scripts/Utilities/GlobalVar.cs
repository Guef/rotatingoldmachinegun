﻿using UnityEngine;
using System.Collections;

public class GlobalVar : MonoBehaviour {
    private static GlobalVar instance;
    public GameObject targetWorld;
    public GameObject enemySpawner;
    public GameObject highscore;
    public GameObject highscoreText;
    public int score;
    public int lastHighscore=0;
    private GlobalVar() {
    }
	void Awake () {
        instance = this;
	}
    public static GlobalVar Instance() {
        return instance;
    }
}
