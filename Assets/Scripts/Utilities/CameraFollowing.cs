﻿using UnityEngine;
using System.Collections;

public class CameraFollowing : MonoBehaviour {
    public float distance;
    public GameObject followed;
    public Vector3 target;
    Vector3 startingPos;
    public float speed;
    void Start() {
        startingPos = transform.position;
    }
	void Update () {
        target = startingPos+ followed.transform.up*distance;
        transform.position=Vector3.MoveTowards(transform.position, target, speed*Time.deltaTime);
	}
}
